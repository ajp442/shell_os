/** @file */
 


#include "dshFunctions.h"


//==============================================================================
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
//                          Core dsh Functions
//
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//==============================================================================

/**************************************************************************//**
 * @author Andrew Pierson
 *
 * @par Description: Returns the Parent Process ID (PPID) of the input 
 * process. Finds the parent via the proc file system. It will look in 
 * /proc/pid/status where pid is the input pid. It will return a zero 
 * if the parent process cannot be found.
 *
 * @param[in] procId - A process.
 *
 * @returns ppid - The parent of the input process. Else, if it is not found
 * return 0.
 *****************************************************************************/
int cmdnm(int procId)
{

    string temp;//Temporary string variable for reading from file.
    long int ppid = -1;  //Stores the parent process one we find it.
    char pidPath[256];//Path to the file we want to read.
    ifstream fin;//Input file stream.


    //Integrate the pid into the file path string.
    snprintf(pidPath, 256, "/proc/%d/status", procId);
                                                     
                                                     
    //Open the file and check that it exists.
    fin.open(pidPath);
    if (!fin)
    {
        cout << "error opening file\n";
    }


    //Read the file line by line until we find the label "PPid:".
    //The next value that we pull in will be the process ID.
    while (fin >> temp)
    {
        if (temp == "PPid:")
        {
            fin >> temp;
            ppid = atol(temp.c_str());//Convert to an integer.
            break;//Break out of the loop one we find the value we were looking for.
        }
    }

    //Close file.
    fin.close();

    //Return what we found. 
    return ppid;
}





/**************************************************************************//**
 * @author Andrew Pierson
 *
 * @par Description: Similar to kill() exept that it will only allow you
 * to send a signal to one process at a time.
 *
 * 
 *
 * @param[in] sig - Signal to send.
 * @param[in] pid - The ID of the process to send the signal to.
 *
 * @returns none
 *****************************************************************************/
void sendSignal(pid_t pid, int sig)
{
    if (sig <= 0)
    {
        cout << "Invalid Signal\n";
    }
    if (pid <= 0)
    {
        cout << "Invalid pid";
    }
    if (kill(pid, sig) != 0)
    {
        printf("ERROR: %s\n",strerror(errno) );
    }

}


/**************************************************************************//**
 * @author Andrew Pierson
 *
 * @par Description: Returns the Linux version as a string. Uses the /proc
 * file-system to extract data out of /proc/version.
 *
 * @returns version_str - A string indicating the current version of the OS.  
 *****************************************************************************/
string getVersion()
{
    string version_str;
    ifstream fin;
    int idx; //Reference index for splitting strings.


    fin.open("/proc/version");
    if(!fin)
    {
        cout << "Error: Cannot open /proc/version" << endl;
    }
    else
    {
        getline (fin, version_str);
        idx = version_str.find(" (");
        version_str.resize(idx);
    }

    //Close the file
    fin.close();

    return version_str;
}

/**************************************************************************//**
 * @author Andrew Pierson
 *
 * @par Description: Looks in /proc/meminfo to find and return pertantant
 * information about the machines memory.
 *
 *
 * @returns result - A string with the resulting memory information we grab
 * from the /proc/meminfo.
 *****************************************************************************/
string getMemInfo()
{
    string memInfo;
    string result;
    ifstream fin;
    int idx; //Reference index for splitting strings.


    fin.open("/proc/meminfo");
    if(!fin)
    {
        cout << "Error: Cannot open /proc/meminfo" << endl;
    }
    else
    {
        while(getline (fin, memInfo) && memInfo.find("Buffer") == string::npos){
            result += memInfo + "\n";
        }
    }

    //Get rid of the lat newline
    result.resize(result.length() - 1);

    //Close the file
    fin.close();
    return result;
}

/**************************************************************************//**
 * @author Andrew Pierson
 *
 * @par Description: Finds and returns all the processes given a command to
 * search for. It will find any process with the given substring.
 *
 * 
 *
 * @param[in] command - The string to search for.
 *
 * @returns procs - A vector of processes to be passed around and printed out.
 *****************************************************************************/
vector<process> findProcesses(string command)
{

    string dir = "/proc";//Root directory to search.
    vector<string> files;//A vector full of all the files that we are going to check.
    string file;//The path to the file we are looking in.
    string comd;//Command read in from file.
    ifstream fin;
    pid_t pid;//Process ID.
    process pr;
    vector<process> procs = vector<process>();//The vector that we are returning.


    //Grab all the files in Dir
    files = getdir(dir);

    for (vector<string>::iterator it = files.begin(); it != files.end(); ++it) 
    {
        file = *it;
        pid = atol(file.c_str());

        //Filter out all the files & dirs that are not numbers.
        //We only want the directories that refer to a process id.
        if (pid != 0)
        {
            //Open every file that corresponds to a pid.
            file = "/proc/" + file + "/comm";
            fin.open(file.c_str());
            if (!fin){cout << "error opening " << file << endl;}
            
            //Opening was successful, now read contents of comm subdirectory.
            else{
                getline(fin, comd);
                //Check to see if the command we find in the file matches 
                //what is given to us.
                if (comd.find(command) != string::npos)
                {
                    //Create a process object and push it onto the vector that we will return.
                    pr.pid = pid;
                    pr.command = comd;
                    procs.push_back(pr);
                }
            }
            fin.close();
        }
    }

    return procs;
}






/**************************************************************************//**
 * @author Andrew Pierson
 *
 * @par Description: Returns a string with the system's uptime formatted
 * in a human readable format. It scales the time it reads from /proc/uptime
 * from seconds to either minutes, hours, or days; whatever is most appropriate.
 *
 * @returns formatedUptime - The uptime of the system in appropriate time 
 * units.
 *****************************************************************************/
string getUptime()
{
    string uptime_str;
    char formatedUptime[256];
    ifstream fin;
    double uptime;
    double idleTime;
    char * pEnd;//Pointer for strtod() function.
    int idx; //Reference index for splitting strings.

    //Open the files that tells us what version of the OS we are running.
    fin.open("/proc/uptime");
    if(!fin)
    {
        cout << "Error: Cannot open /proc/uptime" << endl;
    }
    else
    {
        //Open file read uptime and idle time.
        getline (fin, uptime_str);
        uptime = strtod(uptime_str.c_str(), &pEnd);
        idleTime = strtod(pEnd, NULL);

        //Normalize seconds into something more human readable.
        //Days
        if (uptime/(60*60*24) > 1){
            sprintf(formatedUptime, "%f days", uptime/(60*60*24));
        }
        //Hours
        else if (uptime/(60*60) > 1){
            sprintf(formatedUptime, "%f hours", uptime/(60*60));
        }
        //Minutes
        else if (uptime, uptime/60 > 1){
            sprintf(formatedUptime, "%f", uptime/60);
        }
        //Seconds
        else{
            sprintf(formatedUptime, "%f seconds", uptime);
        }
    }
    fin.close();
    return formatedUptime;
}




/**************************************************************************//**
 * @author Andrew Pierson
 *
 * @par Description: Looks for information about the cpu in /proc/cpuinfo. 
 * Returns a string with this information.
 *
 *
 *
 * @returns cpuInfo_str - The string that contains info about the cpu.
 *****************************************************************************/
string getCpuInfo()
{
    ifstream fin;
    string temp;//Temporary string.
    string cpuInfo_str;//Stores what we will be returning.
    bool keepLooking = true;

    fin.open("/proc/cpuinfo");
    if(!fin)
    {
        cout << "Error: Cannot open /proc/cpuinfo" << endl;
    }
    else
    {
        //Read from the file and grab the fields that we are interested in.
        while (keepLooking)
        {
            getline(fin, temp);

            if (temp.find("processor") != string::npos){
                cpuInfo_str += "\n" + temp + "\n";
            }
            else if (temp.find("model name") != string::npos){
                cpuInfo_str += temp + "\n";
            }
            else if (temp.find("cpu MHz") != string::npos){
                cpuInfo_str += temp + "\n";
            }
            else if (temp.find("cpu cores") != string::npos){
                cpuInfo_str += temp + "\n";

                //We only need to print out info for one core.
                keepLooking = false;
            }
        }
    }
    return cpuInfo_str;
}

/**************************************************************************//**
 * @author Andrew Pierson
 *
 * @par Description: This process will create a pipe to fork a stream from
 * one process into another. A limitation is that it can only handle one
 * pipe.
 *
 * @bug [] Can only handle one pipe.
 * 
 *
 * @param[in] args - A vector of string containing processes to execute.
 * @param[in] pipeloc - Location of the pipe in the vector of arguments.
 *
 * @returns none
 *****************************************************************************/
void pipeAndFork (vector<string> args, int pipeloc)
{
    int fd_pipe[2];//File descriptor pipe.
    int pid1;//Child pid.
    int pid2;//Grand-child pid.
    int status;//Exit status used for debugging.
    int wpid;//Wait pid.
    int i;//Iterators for traversing through vector of arguments.
    int j = 0;
    int nFlags;//Number of flags.
    int nFlags2;//Number of flags for the second process.
    char* chargs[256];//Character arguments for first command.
    char* chargs2[256] = {'\0'};//Character arguments for second command.


    for (i=0; i<pipeloc; i++)
    {
        chargs[j] = (char *)args[i].c_str();
        j++;
    }
    nFlags = j;//Record the number of flags for the first process.

    i++;//Increment over the pipe symbol.
    j = 0;//Reset j for chargs2 to start at 0.

    for (i; i<args.size(); i++)
    {
        chargs2[j] = (char *)args[i].c_str();
        j++;
    }
    nFlags2 = j;//Record the number of flags for the second process.


    pid1 = fork();
    if (pid1 == 0)
    {
        //Create a pipe here so both children will be able to share
        //file descriptors.
        pipe(fd_pipe);// create pipe

        //The child now forks another process. It is amazing how fast they
        //grow up.
        pid2 = fork();
        if (pid2 == 0)
        {
            // grandchild process executes here for output side of pipe
            close(1);              // close standard output
            dup(fd_pipe[1]);       // redirect the output
            close(fd_pipe[0]);     // close unnecessary file descriptor
            close(fd_pipe[1]);     // close unnecessary file descriptor
            //If there are any flags we can do an execvp.
            if(nFlags > 1){execvp(chargs[0], chargs);}
            //Otherwise do an execlp for no flags.
            else{execlp(chargs[0], chargs[0], NULL);}
            cout << "exec failed" << endl;
            exit(1);
        }

        // back to process for input side of pipe
        else
        {
            close(0);              // close standard input
            dup(fd_pipe[0]);       // redirect the input
            close(fd_pipe[0]);     // close unnecessary file descriptor
            close(fd_pipe[1]);     // close unnecessary file descriptor
            //If there are any flags we can do an execvp.
            if(nFlags2 > 1){execvp(chargs2[0], chargs2);}
            //Otherwise do an execlp for no flags.
            else{execlp(chargs2[0], chargs2[0], NULL);}
            cout << "exec failed" << endl;
            exit(1);
        }
    }
    else
    {
        // parent process executes here
        wpid = wait(&status);
        //cout << "The child process id number is " << pid1 
        //<< " [" << wpid << "]." << endl;
        //wpid = wait(&status);
        //cout << "The child process id number is " << pid1 
        //<< " [" << wpid << "]." << endl;
    }
}




/**************************************************************************//**
 * @author Andrew Pierson
 *
 * @par Description: Redirects contents of a file into a command.
 *
 *
 * @param[in] args - Aguments that tell what command to execute.
 * @param[in] greaterThankLoc - Location fo the < symbol in the args vector.
 *
 * @returns none
 *****************************************************************************/
void fileReDirectOut(vector<string> args, int greaterThanLoc)
{
    int i;
    int pid;
    int fpt;//File pointer.
    int wpid;
    int status;
    char* chargs[256];//Character arguments.
    char* fileName;//Name of the file.

    //Convert to character arguments.
    for (i=0; i<greaterThanLoc; i++)
    {
        chargs[i] = (char *)args[i].c_str();
    }

    //Find the filename.
    fileName = (char *)args[greaterThanLoc + 1].c_str();

    pid = fork();
    if (pid == 0)
    {   
        //Open a file for writing, if it does not exist, create it.
        //permission are r w User; r w Group; r w Other
        if ((fpt = open(fileName, O_CREAT|O_WRONLY|O_TRUNC, S_IRUSR|S_IWUSR | S_IRGRP|S_IWGRP | S_IROTH|S_IWOTH)) == -1)
        {   
            printf("Unable to open %s for writing.\n", fileName);
            return;
        }
        close(1);       // close child standard output 
        dup(fpt);      // redirect the child output 
        close(fpt);    // close unnecessary file descriptor
        //If there are any flags we can do an execvp.
        if(greaterThanLoc > 1){execvp(chargs[0], chargs);}
        //Otherwise do an execlp for no flags.
        else{execlp(chargs[0], chargs[0], NULL);}
        cout << "exec failed" << endl;
        exit(1);
    }   

    else
    {   
        wpid = wait(&status);
        cout << "child " << wpid << " exited with status: " << status << endl;
        /* parent process executes here */
        cout << "The child process id number is " << pid << endl;
    }   
}

/**************************************************************************//**
 * @author Andrew Pierson
 *
 * @par Description: Redirect contents of a file into a command.
 *
 *
 * @param[in] args - The arguments and commands that will be executed.
 * @param[in] lessThanLoc - Location of the "<" symbol.
 *
 * @returns none
 *****************************************************************************/
void fileReDirectIn(vector<string> args, int lessThanLoc)
{
    int i;
    int pid;
    int fpt;//File pointer
    int wpid;
    int status;
    char* chargs[256];//Character arguments.
    char* fileName;//The filename.


    //Convert the string in the vector to char*.
    for (i=0; i<lessThanLoc; i++)
    {
        chargs[i] = (char *)args[i].c_str();
    }

    //Find the filename, it will come after the less than sign.
    fileName = (char *)args[lessThanLoc + 1].c_str();

    pid = fork();
    if (pid == 0)
    {

        //Open the file and get the pointer.
        if ((fpt = open(fileName, O_RDONLY)) == -1)
        {
            printf("Unable to open %s for reading.\n", fileName );
            return;
        }
        close(0);// close child standard input
        dup(fpt);// redirect the child input
        close(fpt);// close unnecessary file descriptor
        //If there are any flags we can do an execvp.
        if(lessThanLoc > 1){execvp(chargs[0], chargs);}
        //Otherwise do an execlp for no flags.
        else{execlp(chargs[0], chargs[0], NULL);}
        cout << "exec failed" << endl;
        exit(1);
    }
    else
    {
        /* parent process executes here */
        wpid = wait(&status);
        //cout << "child " << wpid << " exited with status: " << status << endl;
        //cout << "The child process id number is " << pid << endl;
    }
}



/**************************************************************************//**
 * @author Andrew Pierson
 *
 * @par Description: Executes a command and also outputs information about
 * the process that was created. The pid of the child process is printed
 * after the prompt. The usage statement that is printed includes the
 * time for user and system calls, and page faults & swaps.  * 
 *
 * @bug [] After typing an invalid command then a valid command the times
 * returned are 0.
 *
 * @param[in] args - A string vector of arguments to execute.
 *
 * @returns item - none
 *****************************************************************************/
void execCmd(vector<string> args)
{
    int childPid;
    int waitPid;//process id. 
    int status;
    int fd_pipe[2];
    int i;//Iterator.
    int j = 0;
    int n_args;
    char *chargs[512];//Arguments in character array format.
    char buffer [BUFSIZ + 1];
    struct rusage r_usage;//The rusage struct that we will display info from.
    struct rusage curr_r_usage;//Stores the most up-to-date usage information.
    struct rusage prev_r_usage;//Keeps the previous iteration of the usage information.
    n_args = args.size();

    //Just in casse, make sure the imes are set to 0.
//    r_usage.ru_utime.tv_sec = 0;
//    r_usage.ru_utime.tv_usec = 0;
//    r_usage.ru_stime.tv_sec = 0;
//    r_usage.ru_stime.tv_usec = 0;
    
//    curr_r_usage.ru_utime.tv_sec = 0;
//    curr_r_usage.ru_utime.tv_usec = 0;
//    curr_r_usage.ru_stime.tv_sec = 0;
//    curr_r_usage.ru_stime.tv_usec = 0;

//    prev_r_usage.ru_utime.tv_sec = 0;
//    prev_r_usage.ru_utime.tv_usec = 0;
//    prev_r_usage.ru_stime.tv_sec = 0;
//    prev_r_usage.ru_stime.tv_usec = 0;

    for(i=0; i<n_args; i++)
    {
        //Convert the args that were passed in into (char *) so
        //we can feed them to execvp().
        chargs[j] = (char *)args[i].c_str();
        j++;
    }

    //Fork a child process that will execute the command.
    childPid = fork();
    if (childPid == -1)
    {
        fprintf(stdout, "fork failure");
        exit(1);
    }
    //Child will attempt to exec the function.
    if (childPid == 0)//If we are in the child.
    {
        //Print the child pid.
        printf("CHILD PID: %d\n", getpid());
        //Execute the process that the user input.
        execvp(chargs[0], chargs);
        //Anything after this is an error.
        perror("Exec failed: ");
        exit(1);

    }
    //The parent will wait for the child and then print out useful information
    //about it's execution.
    else 
    {
        //Get usage information from the child process that was executed.
        waitPid = wait(&status);
        cout << "we were waiting for: " << waitPid << endl;
        prev_r_usage = curr_r_usage;
        getrusage(RUSAGE_CHILDREN, &curr_r_usage);
        r_usage = subtract_rusage(curr_r_usage, prev_r_usage);

        display_rusage(r_usage);

        //Exit status.
        printf("Shell process %d exited with status %d\n", waitPid, (status >> 8));

    }
    //Clear the character arguments.
    for (i=0; i<256; i++)
    {
        chargs[i] = NULL;
    }
}


/**************************************************************************//**
 * @author Andrew Pierson
 *
 * @par Description: Remote shell pipe server. This function opens a port
 * and waits for a client to connect. It will then write data. Usage:
 * cmd1 )) <port>
 *
 *
 * @param[in] args - The arguments containing the program to run.
 * @param[in] dblOpnParenLoc - Location of the double open parenthesis which
 * indicates to initiate a remote server.
 *
 * @returns none
 *****************************************************************************/
void remoteServe(vector<string> args, int dblClsParenLoc)
{
    int listenfd = 0;//The socket we are listening on.
    int newsockfd = 0;//A new socket to send info to.
    int portno;//Port Number/
    int n_args;//Number of arguments.
    int clilen;//Length of client address.
    int pid;
    int wpid;
    int status;
    int i;//Iterator.
    char* chargs[512];
    struct sockaddr_in serv_addr;//The address of the current machine.
    struct sockaddr_in cli_addr;
    n_args = args.size();//Number of arguments.

    //Make sure there are enough arguments.
    if ( dblClsParenLoc + 2 != n_args)
    {
        cout << "Need to specify an IP address and a port: cmd )) <port>\n";
        return;
    }
        

    for(i=0; i<dblClsParenLoc; i++)
    {
        //Convert the args that were passed in into (char *) so
        //we can feed them to execvp().
        chargs[i] = (char *)args[i].c_str();
    }
    i++;//Move past the double open parenthesis.

    //Convert the port number to an integer.
    portno = atoi(args[i].c_str());


    //Create listen file descriptor.
    listenfd = socket(AF_INET, SOCK_STREAM, 0);
    //Clear the serv_addr.
    memset(&serv_addr, '0', sizeof(serv_addr));

    //Set the address for the socket.
    //This is setting up what we are listening for.
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);
    serv_addr.sin_port = htons(portno); 

    //Specifies the address & port on the local side of the connection.
    //Assign a address to our socket.
    bind(listenfd, (struct sockaddr*)&serv_addr, sizeof(serv_addr)); 

    //Listen for connections (no more than 5 in the queue).
    listen(listenfd, 5); 

    //while(1){
    //Accept a connection on the socket.
    //This causes the process to block until a client connects to the server.
    clilen = sizeof(cli_addr);

    //If we wanted to we could
    //pass client adress (and length) by reference so we can send data back to the client.
    //newsockfd = accept(listenfd, (struct sockaddr*)&cli_addr, &clilen); 
    newsockfd = accept(listenfd, NULL, NULL); 

    pid = fork();
    if (pid == 0)
    {   
        close(1);       // close child standard output 
        dup(newsockfd);      // redirect the child output 
        close(newsockfd);    // close unnecessary file descriptor
        if(dblClsParenLoc > 1){execvp(chargs[0], chargs);}
        //Otherwise do an execlp for no flags.
        else{execlp(chargs[0], chargs[0], NULL);}
        cout << "exec failed" << endl;
        exit(1);
    }   

    else
    {   
        /* parent process executes here */
        wpid = wait(&status);
        ////De-bugging print outs.
        //printf("child %d exited with status %d\n", wpid, status);
        //printf("the child process id number is %d\n", pid);
    }   

    //Close the socket.
    close(newsockfd);
    sleep(1);
    //}//End while loop.
}

/**************************************************************************//**
 * @author Andrew Pierson
 *
 * @par Description: Listens to the server for information.
 *
 * @bug [] If there is an unresponsive server there is no timeout. This
 * causes the program to hang indefinitely. The most common case is when
 * there is an invalid ip address.
 *
 * @param[in] args - A vector of arguments that were read in.
 * @param[in] dbOpnParenLoc - location of the double open parenthesis.
 *
 * @returns none
 *****************************************************************************/
void remoteClient(vector<string> args, int dblOpnParenLoc)
{
    int portno;//Port Number/
    int n_args;//Number of arguments.
    int ipLoc;//Location of the ip address in the vector of arguments.
    int i;//Iterator.
    int pid;
    int wpid;
    int status;
    int sockfd;
    char* chargs[512];
    struct sockaddr_in serv_addr;//The address of the current machine.
    n_args = args.size();//Number of arguments.

    //First things first,
    //Make sure there are enough arguments before moving on.
    if ( dblOpnParenLoc + 3 != n_args)
    {
        cout << "Need to specify an IP address and a port: cmd (( <server IP> <port>\n";
        return;
    }

    for(i=0; i<dblOpnParenLoc; i++)
    {
        //Convert the args that were passed in into (char *) so
        //we can feed them to execvp().
        chargs[i] = (char *)args[i].c_str();
    }

    i++;//Move past the double open parenthesis to the IP address.
    ipLoc = i;
    i++;//Move to the port number.
    //Convert the port number to an integer.
    portno = atoi(args[i].c_str());
    

    //Create socket.
    //This is the socket we will be listening to.
    if((sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
        printf("Error : Could not create socket \n");
        return;
    } 

    //Clear serv_addr.
    memset(&serv_addr, '0', sizeof(serv_addr)); 

    //Set address fields.
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(portno); //Host to network short.

    //Convert characters of a human readable ip address into the proper structure for a network address.
    //Returned in serv_addr.sin_addr.
    //Returns 1 on success.
    if(inet_aton(args[ipLoc].c_str(), &serv_addr.sin_addr)<=0)
    //if(inet_pton(AF_INET, argv[1], &serv_addr.sin_addr)<=0)
    {
        printf("Error: invalid IP\n");
        return;
    } 

    //Establish a connection into the server.
    //passes address and port of host.
    if( connect(sockfd, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
    {
       printf("Error : Connect Failed \n");
       printf("error: %s\n", strerror(errno));
       return;
    } 

    pid = fork();
    if (pid == 0)
    {   
        close(0);// Close child standard input. 
        dup(sockfd);// Redirect the child output. 
        close(sockfd);// Close the now unnecessary file descriptor
        if(dblOpnParenLoc > 1){execvp(chargs[0], chargs);}
        //Otherwise do an execlp for no flags.
        else{execlp(chargs[0], chargs[0], NULL);}
        cout << "exec failed" << endl;
        exit(1);
    }   

    else
    {   
        /* parent process executes here */
        wpid = wait(&status);
        //printf("child %d exited with status %d\n", wpid, status);
        //printf("the child process id number is %d\n", pid);
    }   

    //Close the socket.
    close(sockfd);
    sleep(1);

}



//==============================================================================
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
//                          Display Functions
//
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//==============================================================================



/**************************************************************************//**
 * @author Andrew Pierson
 *
 * @par Description: Prints the Linux version, system uptime, memory usage,
 * and cpu information.
 *
 *
 * @returns none  
 *****************************************************************************/
void print_systat()
{
    //String variables that we will print out.
    string version;
    string uptime;
    string memInfo;
    string cpuInfo;

    //Grab the version, uptime, memory info, and cpu info.
    version = getVersion();
    uptime = getUptime();
    memInfo = getMemInfo();
    cpuInfo = getCpuInfo();

    //Output it to the standard output.
    cout << "====Version====\n";
    cout << version << "\n";
    cout << "====Uptime====\n";
    cout << uptime << "\n";
    cout << "====Memory Info====\n";
    cout <<  memInfo << "\n";
    cout << "====cpu Info====";
    cout << cpuInfo;
    
}




/**************************************************************************//**
 * @author Andrew Pierson
 *
 * @par Description: Prints the command and pid of a process object in
 * a nice format.
 *
 *
 * @param[in] proc - A process object that contains a command and pid.
 *
 * @returns none
 *****************************************************************************/
void printProcess(process proc)
{
    cout << proc.command << "\t" << proc.pid << endl;
}




/**************************************************************************//**
 * @author Andrew Pierson
 *
 * @par Description: Prints a vector of processes for humans to read.
 *
 *
 * @param[in] procs - vector of processes that are to be sent to
 * the standard output.
 *
 * @returns none
 *****************************************************************************/
void printProcesses(vector<process> procs)
{
    if (!procs.empty())
    {
        for (vector<process>::iterator it = procs.begin(); it != procs.end(); ++it) 
        {
            printProcess(*it);
        }
    }
    else 
    {
        cout << "no matching process found\n";
    }

}



/**************************************************************************//**
 * @author Andrew Pierson
 *
 * @par Description: Given a process id, it will call cmdnm() wich retrievs
 * the parent process. It will then check for a valid ID then print it to
 * the console, or output an error.
 *
 *
 * @returns none 
 *****************************************************************************/
void print_cmdnm(int procId)
{
    pid_t ppid;//Parent Process ID.
    ppid = cmdnm(procId);
    if (ppid >= 0)
    {
        cout << ppid << endl;
    }
    else
    {
        cout << "Error: Parent process of process " << procId << " not found \n";
    }
}

void display_rusage(rusage r_usage)
{

        //Display the usage information.
        printf("============usage==========\n");
        printf("CPU usage user (seconds): %ld.%06ld\n" ,r_usage.ru_utime.tv_sec, r_usage.ru_utime.tv_usec);
        printf("CPU usage system (seconds): %ld.%06ld\n" ,r_usage.ru_utime.tv_sec, r_usage.ru_utime.tv_usec);
        printf("page reclaims (soft page faults): %ld\n", r_usage.ru_majflt);
        printf("page faults (hard page faults): %ld\n", r_usage.ru_nswap);
        printf("swaps: %ld\n", r_usage.ru_inblock);
        //    printf("max resident set size: %d\n", r_usage.ru_maxrss);
        //    printf("integral shared memory size: %d\n", r_usage.ru_ixrss);
        //    printf("integral unshared data size: %d\n", r_usage.ru_idrss);
        //    printf("integral unshared stack size: %d\n", r_usage.ru_isrss);
        //    printf("integral unshared stack size: %d\n", r_usage.ru_minflt);
        //    printf("block input operations: %d\n", r_usage.ru_oublock);
        //    printf("block output operations: %d\n", r_usage.ru_msgsnd);
        //    printf("IPC messages sent: %d\n", r_usage.ru_msgrcv);
        //    printf("IPC messages recived: %d\n", r_usage.ru_nsignals);
        //    printf("voluntary context switches: %d\n", r_usage.ru_nvcsw);
        //    printf("involuntary context switches: %d\n", r_usage.ru_nivcsw);
        //    printf("\n");
}


//==============================================================================
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
//                          General Purpose Functions
//
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//==============================================================================




/**************************************************************************//**
 * @author Andrew Pierson
 *
 * @par Description: Extracts the contents (files and directories) of
 * a given directory. Returns a vector of all the names of these files.
 *
 * 
 *
 * @param[in] dir - Name of the directory to search.
 *
 * @returns files - A list of all the files (and directories) that were found.
 * Will return an empty vector if none are found.
 *****************************************************************************/
vector<string> getdir(string dir)
{
    DIR *dp;//Pointer to a directory.
    struct dirent *dirp;//structure of accessing directory stuff.
    vector<string> files = vector<string>();//Vector of file names that will be returned.


    //Try to open the directory.
    if((dp  = opendir(dir.c_str())) == NULL) 
    {
        cout << "Error(" << errno << ") opening " << dir << endl;
        return files;//Return an empty vector.
    }

    //Read in contents of the directory and push them onto the vector that will be returned.
    while ((dirp = readdir(dp)) != NULL) {
        files.push_back(string(dirp->d_name));
    }

    //Close the directory.
    closedir(dp);

    return files;

}



/**************************************************************************//**
 * @author Evan Teran 
 *
 * @par Description: Devides an input string into sections based on a 
 * delimiter. These sections are stored in a vector. The address to
 * that vector is returned.
 *
 * @param[in] s - Address of the string that we want to split.
 * @param[in] delim - Character that we want to break on.
 * @param[in, out] elems - Address of the vector that we want to fill.
 *
 * @returns elems - The address of a vector that was passed in. This vector
 * will be filled with the split up sections of the input string.
 *****************************************************************************/
vector<string> &split(const string &s, char delim, vector<string> &elems)
{
    stringstream ss(s);
    string item;
    while(getline(ss, item, delim))
    {
        elems.push_back(item);
    }
    return elems;
}

/**************************************************************************//**
 * @author Evan Teran 
 *
 * @par Description: Devides an input string into sections based on a 
 * delimiter. These sections are stored in a vector.
 *
 * @param[in] s - Address of the string that we want to split.
 * @param[in] delim - Character that we want to break on.
 *
 * @returns elems - A vector containing split up sections of the input string.
 *****************************************************************************/
vector<string> split(const string &s, char delim)
{
    vector<string> elems;
    return split(s, delim, elems);
}




/**************************************************************************//**
 * @author Andrew Pierson
 *
 * @par Description: Finds the difference between two rusage structs.
 *
 * @bug [] Only tested for user and system time. (no other fields).
 * 
 *
 * @param[in] curr_r_usage - The current usage containing larger time values.
 * @param[in] prev_r_usage - The previous usage.
 *
 * @returns result - The difference between the two rusage structures.
 *****************************************************************************/
rusage subtract_rusage(rusage curr_r_usage, rusage prev_r_usage)
{
    struct rusage result;
    result.ru_utime = timeval_subtract(curr_r_usage.ru_utime, prev_r_usage.ru_utime);
    result.ru_stime = timeval_subtract(curr_r_usage.ru_stime, prev_r_usage.ru_stime);

    return result;
}


/**************************************************************************//**
 * @author Andrew Pierson
 *
 * @par Description: Subtracts two timeval structures, results in the 
 * time difference between the two. timeval is broken
 * up into two parts; seconds and micro seconds.
 *
 *
 * @param[in] end - The ending time.
 * @param[in] start - The starting time.
 *
 * @returns result - The difference between the start time and end time.
 *****************************************************************************/
struct timeval timeval_subtract(struct timeval end, struct timeval start)
{
    struct timeval result;
    long int sec = 0;
    long int usec = 0;
    bool usec_ovflow = false;


    //We want positive numbers, so chedk if the start time is larger than the end time.
    if(start.tv_usec > end.tv_usec)
    {
        //cout << "start time greater than end time" << endl;
        usec = (1000000 + end.tv_usec) - start.tv_usec;
        usec_ovflow = true;//Mark using flag so we can propigat up to seconds.
    }
    else 
    {
        usec = end.tv_usec - start.tv_usec;
    }

    sec = end.tv_sec - start.tv_sec;

    if (usec_ovflow)
        sec = sec - 1;

    result.tv_sec = sec;
    result.tv_usec = usec;

    return result;
}

/**************************************************************************//**
 * @author Andrew Pierson
 *
 * @par Description: Searches a vector of strings to see if it contains
 * a particular word. This function returns the first location of that word.
 * Returns 0 if not found. (Also returns 0 if it is in the 0th position.
 *
 *
 * @param[in] word - The string that we are searching for.
 * @param[in] vect - The vector to search.
 *
 * @returns i - Location of the word.
 * @returns 0 - String not found (or in 0th position).
 *****************************************************************************/
int contains (string word, vector<string> vect)
{
    int i;//Iterator.

    for (i=0; i<vect.size(); i++)
    {
        if (vect[i] == word)
        {
            return i;
        }
    }

    return 0;
}

