/** @file */

#ifndef __MAILBOXFUNCTIONS__H__
#define __MAILBOXFUNCTIONS__H__

#include <sys/sem.h>
#include <sys/ipc.h>

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <algorithm>
#include <semaphore.h>
#include <errno.h>
#include <algorithm>

#define OFFICE_KEY 2999
#define PID_MAX 32768
#define MBOX_MAX 50

#define MUTEX_KEY 12345
#define WRT_KEY 12346
#define HALT_FOR_WRITE_KEY 12347
#define COUNT_KEY 12348

//#define DEBUG 1

///Structure used for the central mailbox.
struct postoffice
{
    int office_shmid;///This stores the shid of the post-office, stored to avoid shmget() calls
    int originalProcess;
    key_t key[MBOX_MAX];///This the key to each mailbox
    int size[MBOX_MAX];///The size of each mailbox
    int shmid[MBOX_MAX];///The shared memory ID for each mailbox.
};


union semun///For IPC system V semaphores.
{
    int val;
    struct semid_ds* buf;
    unsigned short* array;
    struct seminfo* __buf;
};

    //Core Mailbox functions
    int mboxinit(int num_boxes, int size);
    int mboxread(int boxnumber);
    int mboxwrite(int boxnumber);
    int mboxcopy(int boxSource, int boxDestination);
    void mboxdel(bool forcefull);

    //Helper Functions
    postoffice* connectToPostoffice();
    void detachPostoffice(struct postoffice* office);
    void* openMailbox(postoffice* office, int boxnumber);

    //Debugging/Back-door Functions 
    void printShmStats(struct shmid_ds shmbuffer);
    void printBits(size_t const size, void const * const ptr);
    void mboxinfo(int boxnumber);
    void cleartickets();

    //Resource Controll Functions
    void initSems_zero(int key);
    void initSems_one(int key);
    void createInverseMutexSemaphores(int key);
    void countUp(int key, int boxnum);
    void countDown(int key, int boxnum);
    int get_Value(int key, int boxnum);
    void my_wait(int key, int boxnum);
    void my_post(int key, int boxnum);
    void start_ReadProtection(int boxNum);
    void end_ReadProtection(int boxNum);
    void start_WriteProtection(int boxNum);
    void end_WriteProtection(int boxNum);
    void initSemaphores();
    int removesem(int key);
    
    

#endif
