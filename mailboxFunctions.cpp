/** @file */

#include "mailboxFunctions.h"



//==============================================================================
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
//                          Core Mailbox Functions
//
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//==============================================================================

/**************************************************************************//**
 * @author Andrew Pierson
 *
 * @par Description: Creates num_boxes each with size kB of memory. It also
 * creates a "postoffice" central storage space which stores information
 * about all the other mailboxes.
 *
 * @bug [Fixed] Probably takes up too much space for the common mailbox.
 * 
 *
 * @param[in] num_boxes - Number of mailboxes to create.
 * @param[in] size - Size of each mailbox.
 *
 * @returns -1 - Failed.
 * @returns 0 - Success
 *****************************************************************************/
int mboxinit(int num_boxes, int size)
{
    int kibibytes;
    void *shared_memory = (void *)0;
    struct postoffice *office;
    int shmid;
    key_t mailbox_key;
    int i = 0;
    int boxnum = 1;

    if (size <= 0)
    {
        fprintf(stderr, "Mailboxes must have a size larger than zero\n");
        return -1;
    } 
    //Create the "post-office" mailbox //AKA the one that contains information for all to access.
    //Only create one of these for sure.
    shmid = shmget((key_t)OFFICE_KEY, sizeof(struct postoffice), 0666 | IPC_CREAT | IPC_EXCL);
    if (shmid == -1){
        fprintf(stderr, "shmget failed mailboxex may already be initialized.\n");
        return -1;
    }   

    shared_memory = shmat(shmid, (void *)0, 0); 
    if (shared_memory == (void *)-1){
        fprintf(stderr, "shmat failed while creating mailbox\n");
        return -1;
    }   

    office = (struct postoffice *)shared_memory;
    office->office_shmid = shmid;
    office->originalProcess = getpid();

    //Initialize the size of all mailboxes to 0
    for (i = 0; i<num_boxes; i++)
    {
        office->size[i] = 0;

    }



    //Create all other mailboxes
    kibibytes = size*1024;
    mailbox_key = 3000;//Start at a large number.
    for (i = 0; i<num_boxes; i++)
    {

        printf("Creating mailbox %d\n", i);

        //Get shared memory.
        shmid = shmget(mailbox_key, kibibytes, 0666 | IPC_CREAT);
        if (shmid == -1){
            fprintf(stderr, "shmget failed while creating mailbox\n");
            return -1;
        }   


        office->shmid[i] = shmid;
        office->key[i] = mailbox_key;
        office->size[i] = kibibytes;

        mailbox_key++;
    }

    //Detach from post office.
    detachPostoffice(office);

    initSemaphores();

    return 0;
}






/**************************************************************************//**
 * @author Andrew Pierson
 *
 * @par Description: Reads data from the specified mailbox and prints it
 * out to the user.
 *
 *
 * @param[in] boxnumber - The mailbox to read from.
 *
 * @returns -1 - Indicates Failure.
 * @returns 0 - Indicates Success.
 *****************************************************************************/
int mboxread(int boxnumber)
{
    void *shared_memory = (void *)0;
    char *s;
    struct postoffice* office;
    int shmid;
    struct shmid_ds shmdata;
    int size;
    int pid;
    key_t key;

    pid = getpid();

    //Connect to Postoffice
    office = connectToPostoffice();

    //Connect to mailbox
    shared_memory = openMailbox(office, boxnumber);
    if (shared_memory == NULL)
    {
        fprintf(stderr, "Error opening mailbox\n");
        return -1;
    }
    detachPostoffice(office);


    s = (char *)shared_memory;

    //Start Critical Section.
#if !DEBUG
    start_ReadProtection(boxnumber);
#endif
    //Read From Mailbox
    while (*s != (char)NULL)
    {
        printf("%c", *s);
        *s++;
    }
#if !DEBUG
    end_ReadProtection(boxnumber);
#endif
    //End Critical Section.

    printf("\n");

    if (shmdt(shared_memory) == -1) {
        fprintf(stderr, "shmtd failed\n");
        return -1;
    }   
    return 0;
}


/**************************************************************************//**
 * @author Andrew Pierson
 *
 * @par Description: Prompts the user to write data to the specified mailbox.
 *
 * @bug [] The user has to press ctrl-D twice to write data. (they should
 * only have to press it once).
 * 
 *
 * @param[in] boxnumber - The mailbox number to write to.
 *
 * @returns -1 - Indicates Failure.
 * @returns 0 - Indicates Success.
 *****************************************************************************/
int mboxwrite(int boxnumber)
{
    void *shared_memory = (void *)0;
    char *s;
    struct postoffice *office;
    int shmid;
    struct shmid_ds shmdata;
    char buffer[BUFSIZ];
    int size;
    key_t key;
    int result;
    int usedSlots = 0;
    int i = 0;
    int pid;

    pid = getpid();

    //Connect to Postoffice and get the information that we need from it.
    office = connectToPostoffice();
    //Open the requested mailbox
    shared_memory = openMailbox(office, boxnumber);
    if (shared_memory == NULL)
    {
        fprintf(stderr, "Error opening mailbox before writing\n");
        return -1;
    }
    size = office->size[boxnumber];
    detachPostoffice(office);

    //Write Data to mailbox
    s = (char *)shared_memory;
    memset(buffer, (char)NULL, BUFSIZ);//Flush the buffer
    result = fread(buffer, sizeof(char), BUFSIZ, stdin);
    printf("\n");

    //Start Critical Section.
#if !DEBUG
    start_WriteProtection(boxnumber);
#endif
    //Move up to a empty spot.
    usedSlots = 0;
    while (*s != (char)NULL)
    {
        *s++;
        usedSlots++;
    }

    //Write until we have emptied the buffer or we run out of space.
    i = 0;
    while (buffer[i] != (char)NULL && i < size - usedSlots)
    {
        s[i] = buffer[i];
        i++;
    }
#if !DEBUG
    end_WriteProtection(boxnumber);
#endif
    //End Critical Section.

    if (shmdt(shared_memory) == -1) {
        fprintf(stderr, "shmtd failed\n");
        return -1;
    }   


    return 0;
}


/**************************************************************************//**
 * @author Andrew Pierson
 *
 * @par Description: Coppies contents of one mailbox to another. The 
 * destination mailbox will be overwritten.
 *
 *
 * @param[in] boxSource - The mailbox to copy from.
 * @param[in] boxDestination - The mailbox having stuff copied to it.
 *
 * @returns -1 - Indicates Failure.
 * @returns 0 - Indicates Success.
 *****************************************************************************/
int mboxcopy(int boxSource, int boxDestination)
{
    void *shared_memory_source = (void *)0;
    void *shared_memory_dest = (void *)0;
    struct postoffice* office;
    char* source;
    char* dest;
    int pid;
    int so_sz;
    int dst_sz;
    int sm_sz;
    int i = 0;

    pid = getpid();


    //Connect to Postoffice
    office = connectToPostoffice();


    //Connect to source mailbox
    shared_memory_source = openMailbox(office, boxSource);
    if (shared_memory_source == NULL)
    {
        fprintf(stderr, "Error opening mailbox before copying\n");
        return -1;
    }
    source = (char*)shared_memory_source;

    //Connect to destination mailbox
    shared_memory_dest = openMailbox(office, boxDestination);
    if (shared_memory_dest == NULL)
    {
        fprintf(stderr, "Error opening mailbox before copying\n");
        return -1;
    }
    dest = (char*)shared_memory_dest;

    //Copy contents
    so_sz = office->size[boxSource];
    dst_sz = office->size[boxDestination];
    //Only go up to the size of the smallest mailbox.
    sm_sz = (so_sz > dst_sz) ? dst_sz : so_sz;

    detachPostoffice(office);


    //Start Critical section.
#if !DEBUG
    start_ReadProtection(boxSource);
    start_WriteProtection(boxDestination);
#endif
    while (source[i] != (char)NULL && i < sm_sz)
    {
        dest[i] = source[i];
        i++;
    }
    while (i<dst_sz)
    {
        dest[i] = (char)NULL;
        i++;
    }
#if !DEBUG
    end_ReadProtection(boxSource);
    end_WriteProtection(boxDestination);
#endif
    //End Critical section.

    //Detach mailboxes now that we are done copying.
    if (shmdt(shared_memory_source) == -1) {
        fprintf(stderr, "failed to detach source\n");
    }   
    if (shmdt(shared_memory_dest) == -1) {
        fprintf(stderr, "failed to detach destination\n");
    }   

    return 0;
}


/**************************************************************************//**
 * @author Andrew Pierson
 *
 * @par Description: Deletes mailboxes plus the postoffice and semaphores.
 * Only the creator can delete shared resources unless the forceful flag 
 * is set to true.
 *
 * @param[in] forcefull - Set to true to forefully remove mailboxes.
 *
 * @returns none
 *****************************************************************************/
void mboxdel(bool forcefull)
{
    void *shared_memory = (void *)0;
    struct postoffice* office;
    int boxnum = 0;
    int shmid;

    //Connect to Postoffice
    office = connectToPostoffice();
    if (office == NULL)
        return;

    //Make sure we are the original process.
    if (office->originalProcess == getpid() || forcefull)
    {
        printf("removing all mailboxes\n");
        //For every mailbox that is in use: delete it.
        while (office->size[boxnum] != 0)
        {
            printf("Deleting mailbox %d\n", boxnum);
            shared_memory = openMailbox(office, boxnum);
            if (shared_memory == NULL)
            {
                fprintf(stderr,"error while deleting mailbox\n");
                return;
            }

            if (shmdt(shared_memory) == -1) {
                fprintf(stderr, "shmdt failed while deleting mailboxes\n");
                return;
            }   

            if (shmctl(office->shmid[boxnum], IPC_RMID, NULL) == -1){
                fprintf(stderr, "shmctl(IPC_RMID) failed\n");
                return;
            }

            boxnum++;
        }

        //Delete the postoffice itself.
        if (shmctl(office->office_shmid, IPC_RMID, 0) == -1){
            fprintf(stderr, "office shmctl(IPC_RMID) failed\n");
        }

        //Remove the Semaphores, we are done using them.
        removesem(MUTEX_KEY);
        removesem(WRT_KEY);
        removesem(COUNT_KEY);
        removesem(HALT_FOR_WRITE_KEY);
    }
    //Now detach it.
    detachPostoffice(office);

}

















































//==============================================================================
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
//                         Helper Functions 
//
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//==============================================================================

/**************************************************************************//**
 * @author Andrew Pierson
 *
 * @par Description: A helper function that connects to the postofice (i.e.
 * the central mailbox that stores info for all the other mailboxes). This 
 * function returns a pointer the shared memory location of the central
 * mailbox.
 *
 *
 * @returns office - A pointer to the central mailbox.
 *****************************************************************************/
postoffice* connectToPostoffice()
{
    void *shared_memory = (void *)0;
    struct postoffice *office;
    int shmid;
    struct shmid_ds shmdata;

    shmid = shmget((key_t)OFFICE_KEY, 0, 0);
    if (shmid == -1){
        //fprintf(stderr, "postoffice shmget failed\n");
        return NULL;
    }   

    shared_memory = shmat(shmid, (void *)0, 0); 
    if (shared_memory == (void *)-1){
        fprintf(stderr, "postoffice shmat failed\n");
        return NULL;
    }   

    office = (struct postoffice *)shared_memory;
    office->office_shmid = shmid;

    return office;
}

/**************************************************************************//**
 * @author Andrew Pierson
 *
 * @par Description: A nice wrapper for detaching from the central mailbox.
 *
 * @param[in] office - A postoffice object to disconnect from.
 *
 * @returns none
 *****************************************************************************/
void detachPostoffice(struct postoffice* office)
{

    if (shmdt((void *)office) == -1) {
        fprintf(stderr, "failed to detach postoffice\n");
    }   
}

/**************************************************************************//**
 * @author Andrew Pierson
 *
 * @par Description: Will open (get and attach to) a given mailbox number. 
 *
 * 
 *
 * @param[in] office - Central mailbox for getting mailbox information to
 * open the said mailbox.
 * @param[in] boxnumber - Which mailbox to connect to.
 *
 * @returns shared_memory - A void pointer to the location of the mailbox.
 *****************************************************************************/
void* openMailbox(postoffice* office, int boxnumber)
{
    void *shared_memory = (void *)0;
    char *s;
    int shmid;
    int size;
    key_t key;

    if(office == NULL)
        return NULL;

    size = office->size[boxnumber];
    key = office->key[boxnumber];

    //We will never want to create a new section of shared memory.
    shmid = shmget(key, 0, 0);
    if(shmid != office->shmid[boxnumber])
    {
        fprintf(stderr, "wrong mailbox\n");
        return NULL;
    }

    if (shmid == -1){
        fprintf(stderr, "shmget failed while opening mailbox\n");
        return NULL;
    }

    shared_memory = shmat(shmid, (void *)0, 0);
    if (shared_memory == (void *)-1){
        fprintf(stderr, "shmat failed while opening mailbox\n");
        return NULL;
    }


    return shared_memory;
}












//==============================================================================
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
//                         Debugging/Back-door Functions
//
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//==============================================================================

/**************************************************************************//**
 * @author Andrew Pierson
 *
 * @par Description: A helper function that prints out stats. Used by mboxinfo()
 *
 *
 * @param[in] shmbuffer - A shmid_ds data structure used for getting info from.
 *
 * @returns none
 *****************************************************************************/
void printShmStats(struct shmid_ds shmbuffer)
{
    printf ("segment size: %d\n", shmbuffer.shm_segsz); 
    printf ("owner euid: "); 
    printBits(sizeof(shmbuffer.shm_perm.uid), &shmbuffer.shm_perm.uid);
    printf ("owner egid: "); 
    printBits(sizeof(shmbuffer.shm_perm.gid), &shmbuffer.shm_perm.gid);
    printf ("creator euid: "); 
    printBits(sizeof(shmbuffer.shm_perm.uid), &shmbuffer.shm_perm.cuid);
    printf ("creator egid: "); 
    printBits(sizeof(shmbuffer.shm_perm.gid), &shmbuffer.shm_perm.cgid);
    printf ("access modes:  %o\n", shmbuffer.shm_perm.mode); 
    printf ("pid of creator: %d\n", shmbuffer.shm_cpid);
    printf ("number of current attaches: %d\n", shmbuffer.shm_nattch);
    printf ("pid of last operator: %d\n", shmbuffer.shm_lpid);
    printf ("last attach: %s", ctime (&shmbuffer.shm_atime));
    printf ("last detach: %s", ctime (&shmbuffer.shm_dtime));
    printf ("last change: %s", ctime (&shmbuffer.shm_ctime));
}


/**************************************************************************//**
 * @author Andrew Pierson
 *
 * @par Description: A helper function that prints things in binary. Usefull
 * for looking at permissions.
 *
 *
 * @param[in] size - Size of the item that you want printed in binary.
 * @param[in] ptr - Pointer to item you want printed in binary.
 *
 * @returns none
 *****************************************************************************/
void printBits(size_t const size, void const * const ptr)
{
    unsigned char *b = (unsigned char*) ptr;
    unsigned char byte;
    int i, j;

    for (i=size-1;i>=0;i--)
    {
        for (j=7;j>=0;j--)
        {
            byte = b[i] & (1<<j);
            byte >>= j;
            printf("%u", byte);
        }
    }
    puts("");
}





/**************************************************************************//**
 * @author Andrew Pierson
 *
 * @par Description: A function for helping debug and just viewing interesting
 * stats in general.
 *
 * @todo [] Add more information about how much data is written to the mailbox.
 * 
 *
 * @param[in] boxnumber - The mailbox to inspect.
 *
 * @returns none.
 *****************************************************************************/
void mboxinfo(int boxnumber)
{
    struct postoffice* office;
    struct shmid_ds shmdata;

    office = connectToPostoffice();
    shmctl (office->shmid[boxnumber], IPC_STAT, &shmdata); 
    printShmStats(shmdata);
    detachPostoffice(office);
}


























//==============================================================================
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
//                         Resource Control Functions
//
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//==============================================================================


/**************************************************************************//**
 * @author Andrew Pierson
 *
 * @par Description: Initializes a set of semaphores to a value of zero.
 * These are used for counting (non blocking) semaphore or inverse semaphores
 * that are unlocked at zero. 
 *
 * @param[in] key - The identifier for this set of mailboxes.
 *
 * @returns none.
 *****************************************************************************/
void initSems_zero(int key)
{
    int semid;
    semid = semget(key, MBOX_MAX, 0666 | IPC_CREAT | IPC_EXCL);
    if(semid < 0)
    {
        fprintf(stderr, "Unable to obtain counting semaphore.\n");
        return;
    }
    unsigned short values[MBOX_MAX];
    for (int i = 0; i<MBOX_MAX; i++)
    {
        values[i] = 0;
    }

    //For some reason it needs a senum for semctl to work.
    union semun arg;

    arg.array = values;
    if (semctl(semid, 0, SETALL, arg)<0)
    {
        fprintf( stderr, "Cannot set semaphore value.\n");
    }
}

/**************************************************************************//**
 * @author Andrew Pierson
 *
 * @par Description: Initializes a set of semaphores to a value of one. This
 * is used for creating  semaphores that are used as  mutexes. These
 * semaphores start at 1 and are meant to be decremented to 0 so they lock,
 * then incremented again so they unlock.
 *
 * @param[in] key - The identifier for the set of semaphores.
 *
 * @returns none.
 *****************************************************************************/
void initSems_one(int key)
{
    int semid;
    semid = semget(key, MBOX_MAX, 0666 | IPC_CREAT | IPC_EXCL);
    if(semid < 0)
    {
        fprintf(stderr, "Unable to obtain mutex semaphore.\n");
        return;
    }
    unsigned short values[MBOX_MAX];
    for (int i = 0; i<MBOX_MAX; i++)
    {
        values[i] = 1;
    }

    union semun arg;

    arg.array = values;
    if (semctl(semid, 1, SETALL, arg)<0)
    {
        fprintf( stderr, "Cannot set semaphore value.\n");
    }
}


/**************************************************************************//**
 * @author Andrew Pierson
 *
 * @par Description: Increments a semaphore without ever having to wait for it.
 *
 *
 * @param[in] key - Identifier for the set of semaphores to act upon.
 * @param[in] boxnum - The mailbox reader count (or specific semaphore)
 * to increment
 *
 * @returns none.
 *****************************************************************************/
void countUp(int key, int boxnum)
{
    int id;
    struct sembuf ops;
    int retval; 

    //Get the index for the semaphore with external name key.
    id = semget(key, 0, 0666);
    if(id < 0)//Semaphore does not exist. 
    {
        fprintf(stderr, "Program sema cannot find semaphore.\n");
        return;
    }


    //Set up the sembuf structure.
    ops.sem_num = boxnum;//Which semaphore in the semaphore array
    ops.sem_op = 1;//Which operation? Add 1 to semaphore value
    ops.sem_flg = IPC_NOWAIT;//Set the flag so we will wait

    //Do the operation!
    retval = semop(id, &ops, 1);

    if(retval == 0)
    {
    }
    else
    {
        printf("sema: V-operation did not succeed.\n");
        perror("REASON");
    }
}

/**************************************************************************//**
 * @author Andrew Pierson
 *
 * @par Description: Decrements a semaphore without ever having to wait for it.
 *
 *
 * @param[in] key - Identifier for the set of semaphores to act upon.
 * @param[in] boxnum - The mailbox reader count (or specific semaphore)
 * to decrement.
 *
 * @returns none.
 *****************************************************************************/
void countDown(int key, int boxnum){
    int id;
    struct sembuf ops;
    int retval; 

    //Get the index for the semaphore with external name key.
    id = semget(key, 0, 0666);
    if(id < 0)//Semaphore does not exist. 
    {
        fprintf(stderr, "Program sema cannot find semaphore.\n");
        return;
    }

    //Set up the sembuf structure.
    ops.sem_num = boxnum;//Which semaphore in the semaphore array
    ops.sem_op = -1;//Which operation?
    ops.sem_flg = IPC_NOWAIT;//Set the flag so we will not wait

    //Do a semaphore V-operation.
    retval = semop(id, &ops, 1);

    if(retval == 0)
    {
    }
    else
    {
        printf("sema: V-operation did not succeed.\n");
        perror("REASON");
    }
}

/**************************************************************************//**
 * @author Andrew Pierson
 *
 * @par Description: Returns the value of a semaphore. Great for debugging.
 *
 *
 * @param[in] key - Identifier for the set of semaphores to act upon.
 * @param[in] boxnum - Identifies the specific semaphore in the group.
 *
 * @returns none.
 *****************************************************************************/
int get_Value(int key, int boxnum)
{
    int id;
    union semun arg;
    id = semget(key, 0, 0666);
    return semctl(id, boxnum, GETVAL, arg);
}


/**************************************************************************//**
 * @author Andrew Pierson
 *
 * @par Description: Waits for a semaphore if it is zero, otherwise decremetns
 * it's value.
 *
 *
 * @param[in] key - Identifier for the set of semaphores to act upon.
 * @param[in] boxnum - The mailbox reader count (or specific semaphore)
 *
 * @returns none.
 *****************************************************************************/
void my_wait(int key, int boxnum)
{
    int id;
    struct sembuf ops;
    int retval; 

    //Get the index for the semaphore with external name key.
    id = semget(key, 0, 0666);
    if(id < 0)//Semaphore does not exist.
    {
        fprintf(stderr, "Program sema cannot find semaphore.\n");
        return;
    }


    //Set up the sembuf structure. 
    ops.sem_num = boxnum;//Which semaphore in the semaphore array
    ops.sem_op = -1;
    ops.sem_flg = SEM_UNDO;

    //Do the operation.
    retval = semop(id, &ops, 1);

    if(retval != 0)
    {
        printf("sema: V-operation did not succeed.\n");
        perror("REASON");
    }
}

/**************************************************************************//**
 * @author Andrew Pierson
 *
 * @par Description: Increments the value of a semaphore by one.
 *
 *
 * @param[in] key - Identifier for the set of semaphores to act upon.
 * @param[in] boxnum - The mailbox reader count (or specific semaphore)
 *
 * @returns none.
 *****************************************************************************/
void my_post(int key, int boxnum)
{
    int id;
    struct sembuf ops;
    int retval; 

    /* Get the index for the semaphore with external name KEY. */
    id = semget(key, 0, 0666);
    if(id < 0)//Semaphore does not exist.
    {
        fprintf(stderr, "Program sema cannot find semaphore.\n");
        return;
    }


    //Set up the sembuf structure.
    ops.sem_num = boxnum;//Which semaphore in the semaphore array
    ops.sem_op = 1;//Which operation? Add 1 to semaphore value
    ops.sem_flg = SEM_UNDO;//Set the flag so we will wait

    //Do the operation.
    retval = semop(id, &ops, 1);

    if(retval != 0)
    {
        printf("sema: V-operation did not succeed.\n");
        perror("REASON");
    }
}

/**************************************************************************//**
 * @author Andrew Pierson
 *
 * @par Description: Waits for a semaphore if it is non -zero.
 *
 *
 * @param[in] key - Identifier for the set of semaphores to act upon.
 * @param[in] boxnum - The mailbox reader count (or specific semaphore)
 *
 * @returns none.
 *****************************************************************************/
void my_wait_only(int key, int boxnum)
{
    int id;
    struct sembuf ops;
    int retval; 

    //Get the index for the semaphore with external name key.
    id = semget(key, 0, 0666);
    if(id < 0)//Semaphore does not exist.
    {
        fprintf(stderr, "Program sema cannot find semaphore.\n");
        return;
    }


    //Set up the sembuf structure. 
    ops.sem_num = boxnum;//Which semaphore in the semaphore array
    ops.sem_op = 0;//Is unlocked when 0.
    ops.sem_flg = SEM_UNDO;

    //Do the operation.
    retval = semop(id, &ops, 1);

    if(retval != 0)
    {
        printf("sema: V-operation did not succeed.\n");
        perror("REASON");
    }
}

/**************************************************************************//**
 * @author Andrew Pierson
 *
 * @par Description: Starts Reading protection. This is a modified version
 * of the readers first algorithm.
 *
 * @bug [] If there is an extreemly fast writer, readers could be starved.
 * This could be fixed by queuing up a finite number of readers while a write
 * is occuring then letting them through before the next write.
 * 
 *
 * @param[in] boxNum - The mailboxnumber to protect.
 *
 * @returns none
 *****************************************************************************/
void start_ReadProtection(int boxNum)
{
    
    my_wait_only(HALT_FOR_WRITE_KEY, boxNum);//If there is a writer waiting we cannot let any more readers start.
    my_wait(MUTEX_KEY, boxNum);//Mutex to protect changing of variables.
    countUp(COUNT_KEY, boxNum);//Increment the number of readers that are in a mailbox.
    if (get_Value(COUNT_KEY, boxNum) == 1){//The first reader must lock the mailbox from the writers.
        my_wait(WRT_KEY, boxNum);
    }
    my_post(MUTEX_KEY, boxNum);//We are done editing variables, unlock.
}
/**************************************************************************//**
 * @author Andrew Pierson
 *
 * @par Description: Ends Reading protection. This is a modified version
 * of the readers first algorithm.
 *
 *
 * @param[in] boxNum - The mailboxnumber to protect.
 *
 * @returns none
 *****************************************************************************/
void end_ReadProtection(int boxNum)
{

    my_wait(MUTEX_KEY, boxNum);//Get a lock before altering variables.
    countDown(COUNT_KEY, boxNum);//Keep track of the readers that leave the mailbox.
    if (get_Value(COUNT_KEY, boxNum) == 0){//Last one out unlocks the mailbox for the writers.
        my_post(WRT_KEY, boxNum);
    }
    my_post(MUTEX_KEY, boxNum);//Done editing variables.
}

/**************************************************************************//**
 * @author Andrew Pierson
 *
 * @par Description: Starts Write protection. This is a modified version
 * of the readers first algorithm. My version allows a writer to stop
 * readers from entering once a writer is ready.
 *
 *
 * @param[in] boxNum - The mailboxnumber to protect.
 *
 * @returns none
 *****************************************************************************/
void start_WriteProtection(int boxNum)
{
    //An inverse semaphore deal. Allows the writer to hold it's place in queue.
    my_post(HALT_FOR_WRITE_KEY, boxNum);
    my_wait(WRT_KEY, boxNum);//Lock the mailbox for writing.
}

/**************************************************************************//**
 * @author Andrew Pierson
 *
 * @par Description: Ends Write protection. This is a modified version
 * of the readers first algorithm. My version allows a writer to stop
 * readers from entering once a writer is ready.
 *
 *
 * @param[in] boxNum - The mailboxnumber to protect while writing.
 *
 * @returns none
 *****************************************************************************/
void end_WriteProtection(int boxNum)
{
    my_wait(HALT_FOR_WRITE_KEY, boxNum);//Release it's position in the queue.
    my_post(WRT_KEY, boxNum);//Unlock the mailbox for others.
}

/**************************************************************************//**
 * @author Andrew Pierson
 *
 * @par Description: Initializes all the semaphores to the appropriate values.
 *
 *
 * @param[in] boxNum - The mailboxnumber to unlock.
 *
 * @returns none
 *****************************************************************************/
void initSemaphores()
{
    //Create the semaphores that we will need.
    initSems_one(MUTEX_KEY);
    initSems_one(WRT_KEY);
    initSems_zero(HALT_FOR_WRITE_KEY);
    initSems_zero(COUNT_KEY);
}

/**************************************************************************//**
 * @author Andrew Pierson
 *
 * @par Description: Removes all semaphores belonging to a particular key.
 *
 *
 * @param[in] - key - The key to a set of semaphores.
 *
 * @returns id - The ID of the semaphore being removed.
 *****************************************************************************/
int removesem(int key)
{
    int id;
    id = semget(key, 0, 0666);
    if ( semctl (id, 0, IPC_RMID, NULL) == -1)
    {
        printf("error removing semaphore\n");
    }
}






















































