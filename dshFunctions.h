/** @file */

#ifndef __DSHFUNCTIONS__H__
#define __DSHFUNCTIONS__H__


#include <string.h>          //C POSIX library.//Depricated
#include <signal.h>          //C POSIX library.//Depricated
#include <errno.h>           //C POSIX library.//Depricated
#include <stdlib.h>          //C POSIX library.
#include <stdio.h>           //C POSIX library.
#include <unistd.h>          //C POSIX library.
#include <dirent.h>          //C POSIX library.
#include <fcntl.h>           //C POSIX library.
#include <sys/types.h>       //C POSIX library.
#include <sys/wait.h>        //C POSIX library.
#include <sys/time.h>        //C POSIX library.
#include <fstream>           //STL
#include <iostream>          //STL
#include <vector>            //STL
#include <sstream>           //STL
#include <sys/resource.h>    //GNU C library
#include <sys/socket.h>      //GNU C library
#include <netinet/in.h>      //GNU C library
#include <arpa/inet.h>       //GNU C library
#include "mailboxFunctions.h"


using namespace std;



/*!
 * @brief Holds a process ID and its command string.
 */
struct process
{
    pid_t pid;
    string command;
};

//Core dsh functions.
string getUptime();
string getVersion();
string getMemInfo();
string getCpuInfo();
vector<process> findProcesses(string command);
int cmdnm(int procId);
void sendSignal(int pid, int sig);
void execCmd(vector<string> args);
void pipeAndFork (vector<string> args, int pipeloc);
void fileReDirectOut(vector<string> args, int greaterThanLoc);
void fileReDirectIn(vector<string> args, int lessThanLoc);
void remoteServe(vector<string> args, int dblClsParenLoc);
void remoteClient(vector<string> args, int dblOpnParenLoc);

//Display Functions. 
void print_systat();
void print_cmdnm(int procId);
void printPid(string command);
void printProcesses(vector<process> procs);
void printProcess(process proc);
void display_rusage(rusage r_usage);

//Gengeral purpose functions.
vector<string> &split(const string &s, char delim, vector<string> &elems);
vector<string> split(const string &s, char delim);
vector<string> getdir(string dir);
rusage subtract_rusage(rusage curr_r_usage, rusage prev_r_usage);
struct timeval timeval_subtract(struct timeval larger, struct timeval smaller);
int contains (string word, vector<string> vect);

#endif
