# Author: Andrew Pierson
# Date:   01/23/2013


CC = g++

SRC = dsh.cpp
OBJ = dsh.o
EXE = dsh

all: dsh

dsh: dsh.o dshFunctions.o mailboxFunctions.o
	$(CC) dsh.o dshFunctions.o mailboxFunctions.o -pthread -o $(EXE)

dsh.o: dsh.cpp dshFunctions.h dshFunctions.cpp mailboxFunctions.h mailboxFunctions.cpp
	$(CC) -c -g dsh.cpp dshFunctions.cpp mailboxFunctions.cpp

dshFunctions.o: dshFunctions.h dshFunctions.cpp 
	$(CC) -c dshFunctions.cpp

mailboxFunctions.o: mailboxFunctions.h mailboxFunctions.cpp 
	$(CC) -c mailboxFunctions.cpp

clean:
	rm -f *.o $(EXE)

#//this removes temporary files
cleanall: 
	rm -f *.o *.~ $(EXE) 



#to check tabs:
#	 $cat -v -t -e makefile
