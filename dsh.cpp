/*************************************************************************//**
 * @file
 *
 * @brief A command line shell for process identification 
 *
 * @mainpage Overview
 *
 * @section course_section Operating Systems
 *
 * @author Andrew Pierson
 *
 * @date January 19, 2013
 *
 * @par Professor:
 *         Dr. Jeff McGough
 *
 * @par Course:
 *         CSC 456 2:00pm
 *
 * @par Location:
 *         McLaury - 313
 *
 * @section program_section Program Information
 *
 * @details 
 * Overview:
 * It is a command line program with 
 * an executable named dsh. When executed there will be a prompt string: dsh>
 * it will accept commands to be executed. This is essentially a shell similar
 * to bash (But much smaller and less functionality).

 * Part 1:
 * The first part of this program focusses on introducing the unix environment, system
 * calls, signals, and the proc file system. It is a command line program with 
 * an executable named dsh. When executed there will be a prompt string: dsh>
 * it will accept commands to be executed. 
 *
 * Notes on the program architecture: The functions that are in-charge
 * of printing  the results of the commands
 * are kept separate from the functions that extract the information. This
 * promotes some modularity and reduces the size of functions.
 * 
 *
 * Part 2:
 * For the second part of this project most of the focus was on pipes, sockets,
 * and file re-direction.
 *
 * One limitation of this shell right now is that you cannot chain any IO
 * re-direction. For example you cannot use multiple pipes, pipes with
 * file re-direction, or remote shell pipes with either pipes or file 
 * re-direction.
 *
 * Part 3:
 * The third programming assignment is to add a middleware shared memory
 * utility to dsh. This block of shared memory will be available for storage
 * and interprocess communication. The shell will setup the blocks of shared
 * memory and the required semaphores to control access to the shared memory.
 * Since multiple processes will have access to the shared memory, the standard
 * shared resource problems will arise. You must resolve the multiple access
 * (or starvation and deadlock) issues that can occur.
 *
 * Implementation/Mailbox structure: 
 * The user will request X number of mailboxes of size Y Kilobytes.
 * I decided to create X shared memory segments of Y kilobytes instead of
 * creating one large memory segment that I devide up myself. 
 * In order to access each mailbox, or segment of shared memory, I create
 * a centralized segment of memory (which I call the postoffice), that stores
 * information about the mailboxes that are created. The postoffice has a key
 * that is static so all other functions can access it so they can find the shmid, key,
 * or size of a particular mailbox.
 * I set it up this way so it would be easy to dynamically add more mailboxes
 * of varying sizes if the need ever arose.
 *
 * Resource Control Structure:
 * I used IPC semaphores. They are System V complient. They are created in
 * arrays which is a feautre that I found usefull. I used a modified version of
 * the readers prefrerence (aka readers first) algorithem. The typical readers
 * prefreence algorithm allows multiple readers to access a shared memory
 * segment while only allowing one writer to access a shared memory segment at
 * a time. This however is problematic if there are many readers and never let
 * the writer acces the shared memoryresource. To solve this I added a mutex
 * that the writer triggers when it is ready. This bolocks more readers from
 * entering but allows the current readers to finish what they are doing. Once
 * all the readers have left. The writer (wich there is only one of) gets to
 * have a turn. The only downside to this is if the writer is crazy fast and
 * never lets readers in.  I also assume that each writer and reader will only
 * be in the critical 
 * section for the time it takes them to write to, or read from the entire
 * size of the mailbox, thus solving the problem of Bounded Waiting. If we assume
 * that each process can only read/write to the end of a memory segment then we
 * do not have to worry about a reader or a writer taking up all the time using the 
 * resource.
 *
 *
 * @section compile_section Compiling and Usage
 *
 * @par Compiling Instructions:
 *      The files the program relies on are: dsh.cpp, dshFunctions.h, dshFunctions.cpp, mailboxFunctions.h, and mailboxFunctions.cpp
 *      Use Makefile to compile.
 *
 * @par Usage:
 * @verbatim
====COMMANDS====
dsh
    *invokes the program

cmdnm
    *Return the command string (name) that started the process for a given process id
    *Usage: dsh> cmdnm <pid>

signal
    *Sends a signal to a process.
    *Usage: signal <signal_num> <pid>
    
systat
    *Prints Linux version, system uptime, memory usage, and cpu information.
    *This function uses the /proc/* files

pid
    *Returns the process ids for a given command string, matches all substrings.
    *Usage: pid <command>

cd <path>
 *Changes directory to the specified path.

<unix command>
 *Executes any standard unix command and prints usage information.

unix_command > file
 *Re-directs output to the specified file.

unix_command < file
 *Re-directs file contents to the given command.

unix_command | unix_command
 *Pipes output of first command into input of second command.

unix_command )) <port>
 *Sends the output of the unix_command to the specified port. 
 
unix_command (( <server IP> <port>
 *Connects a unix_command to a server and takes input from it. 

mboxinit <number of mailboxes> <size of mailbox in kbytes>
 *Will create k+1 blocks
 *k is the number of mailboxes requested
 *block size = size of mailbox
 *The first block of shared memory will contain the mailbox information for all to access.
 *The next k blocks will act as the mailboxes

mboxdel [-f]
 *Removes mailbox
 *Clean up semaphores and shared memory.  This must be done by the creating shell on exit if not before.
 *The -f flag is used for forcefully removing mailboxes (even if you are not the one who created them).

mboxwrite <box number>
 *Write to mailbox
 *...  data to be written  ...
 *ctrl-d  to end
 *Truncate if data is larger than the mailbox and flag user.
 
mboxread <boxnumber>
 *Read from mailbox.
 *Will read iin the text until end of the string or the end of the mailbox is found.

mboxcopy <boxnumber1> <boxnumber2>
 *Copy contents of one mailbox to another.


exit
    *Terminates the program.

@endverbatim
 *
 * @section todo_bugs_modification_section Todo, Bugs, and Modifications
 *
 *
 * @todo [complete] cmdnm function
 * @todo [complete] signal function
 * @todo [complete] systat function
 * @todo [complete] pid function
 * @todo [complete] exit function
 * @todo [complete] catch and display all signals recived.
 * @todo [complete] accept any command plus arguments, return stdout, and print usage.
 * @todo [complete] implement shell intrinsic functions (cd command).
 * @todo [complete] implement redirection
 * @todo [complete] implement shell pipes
 * @todo [complete] implement remote shell pipes
 * @todo [complete] process statistics
 * @todo [complete] comment and spell check.
 *
 * @bug [FIXED] Central mailbox does not detach when program terminates.
 * @bug [FIXED] The central mailbox takes up lots of room. I would have to re-
 * work my overall architecture to reduce the size.
 * @bug [] Only supports a maximum of 85 mailboxes.
 * @bug [FIXED] There can only be one reader or writer accessing a mailbox at a time.
 * 
 *
 * @par Modifications and Development Timeline:
 * @verbatim
 * Date          Modification
 * ------------  --------------------------------------------------------------
 * Jan 16, 2013  Program #1 assigned
 * Jan 18, 2013  Created project files, read requierments
 * Jan 23, 2013  Made Makefile, split program into different files.
 * Jan 24, 2013  String parsing and cmdnm functions written.
 * Jan 25, 2013  Finished systat function.
 * Jan 26, 2013  Worked on signal function.
 * Jan 27, 2013  Got shell working in a loop until exit command is executed
 *               Spell-checked comments. Got signal working with
 *               error-handeling.
 * Jan 29, 2013  Added signal catching and pid functionallity. 
 *               Tested all functions and finalized documentation.
 * Jan 30, 2013  Final touches, created documentation.
 * Jan 30, 2013  Program #1 due.
 *
 * Feb 08, 2013  Started working on the second part of this project.
 * Feb 11, 2013  Worked on taking any command and print usage satement.
 * Feb 12, 2013  Finished accepting any satement.
 * Feb 13, 2013  Worked on implementing getrusage() function.
 *               Ran into time accumulation problems.
 * Feb 15, 2013  Fixed getrusage() bug. Added functions to subtract time
 *               structures.
 * Feb 17, 2013  Got pipeing working.
 * Feb 18, 2013  Working on file re-direction.
 * Feb 19, 2013  Prototyped remote shell pipes.
 * Feb 20, 2013  Tested each of the new functions, spell checked comments,
 *               created pdf from doxygen.
 * Feb 20, 2013  Program #2 due.
 *
 * Feb 23, 2013  Program #3 assigned.
 * Mar 22, 2013  Started working on project.
 * Mar 24, 2013  Testing out shared memory.
 * Mar 26, 2013  Set up functions that create shared memory segments.
 * Mar 27, 2013  Wrote helper functions for connecting to individual
 * mailboxes.
 * Mar 28, 2013  Wrote functions for bakers algorithm.
 * Mar 29, 2013  Spell checked, commented and tested program.
 * Mar 29, 2013  Program #3 due.
 *
 *
 * @endverbatim
 *
 *****************************************************************************/

#include "dshFunctions.h"
#include "mailboxFunctions.h"



using namespace std;

void signalCatch(int sig);
void signalDelMbox(int sig);



int main(int argc, char *argv[])
{
    string promt = "dsh> ";
    int procId;//Process ID.
    int sig;//Signal.
    pid_t childPid;//Child Process ID.
    char cmd[512];//Variable that a given command is initialy read into.
    bool should_exit = false;//Exit flag to leave the program.
    int i;//Iterator.
    int pipeloc;//Location of the pipe symbol.
    int dblClsParenLoc;//Double close parenthasis location.
    int dblOpnParenLoc;//Double open parenthasis location.
    int greaterThanLoc;//Greater than symbol location.
    int lessThanLoc;//Less than symbol location.
    int result;
    int readresult;
    int numBoxes;
    int size;
    int boxNum;
    int boxSoNum;
    int boxDestNum;
    int pid;
    int val;
    int count;


    vector<process> procs;


    //Usage strings for each of the commands.
    string dsh_usage; 
    string cmdnm_usage; 
    string signal_usage; 
    string systat_usage;
    string pid_usage; 
    string cd_usage;
    string mboxinit_usage; 
    string mboxdel_usage; 
    string mboxwrite_usage; 
    string mboxread_usage; 
    string mboxcopy_usage; 
    string mboxinfo_usage; 
    string exec_usage; 
    string redirectTo_usage;
    string redirectFrom_usage;
    string pipe_usage;
    string server_usage;
    string client_usage;

    //Usage strings for each of the commands.
    dsh_usage += cmdnm_usage = "cmdnm\n\treturns the parent process\' ID. \n\tUsage: cmdnm <pid>\n";
    dsh_usage += "\n";
    dsh_usage += signal_usage = "signal\n\tsends a signal to a process. \n\tUsage: signal <pid> <sig>\n";
    dsh_usage += "\n";
    dsh_usage += systat_usage= "systat\n\tprovides key statistics about the system\n\tUsage: systat\n";//Not used; it does not take any arguments.
    dsh_usage += "\n";
    dsh_usage += pid_usage = "pid\n\tReturns the process ids for a given command string. It will match all substrings\n\t"
                       "Usage: pid <command>\n";
    dsh_usage += "\n";
    dsh_usage += cd_usage= "cd\n\tChanges the current working directory\n";
    dsh_usage += "\n";
    dsh_usage += mboxinit_usage = "mboxinit\n\tCreates number of mailboxes requested + 1. Each with a size requested in kbytes "
                            "The extra block is will contain the mailbox information for all to access.\n\t"
                            "Usage: mboxinit <number of mailboxes> <size of mailbox in kbytes>\n";
    dsh_usage += "\n";
    dsh_usage += mboxdel_usage = "mboxdel\n\tCleans up semaphoes and shared memory. This is also done automatically bo the creating shell on exit if not before.\n\t"
                            "Usage: bmoxdel\n";
    dsh_usage += "\n";
    dsh_usage += mboxwrite_usage = "mboxwrite\n\tWrites data to a mailbox. Data will be truncated and the user will be "
                             "flagged if data being written is lager than the mailbox.\n\t"
                             "Usage: mboxwrite <boxnumber>\n\t"
                             "...data to be written...\n\t"
                             "ctrl-d to end\n";
    dsh_usage += "\n";
    dsh_usage += mboxread_usage = "mboxread\n\tWill read in text until end of the string or end of the mailbox is found.\n\t"
                            "Usage: mboxread <boxnumber>\n";
    dsh_usage += "\n";
    dsh_usage += mboxcopy_usage = "mboxcopy\n\tCopy the contents of one box to another\n\t"
                            "Usage: mboxcopy <boxsource> <boxdestination>\n";
    dsh_usage += "\n";
    dsh_usage += mboxinfo_usage = "mboxinfo\n\tDisplays information about a given mailbox\n\t"
                            "Usage: mboxinfo <boxnumber>\n";
    dsh_usage += "\n";
    dsh_usage += exec_usage = "<unix command>\n\tExecutes any standard unix command and prints usage information.\n"; 
    dsh_usage += "\n";
    dsh_usage += redirectTo_usage = "unix_command > file\n\tRe-directs output to the specified file.\n";
    dsh_usage += "\n";
    dsh_usage += redirectFrom_usage =  "unix_command < file\n\tRe-directs file contents to the given command.\n";
    dsh_usage += "\n";
    dsh_usage += pipe_usage = "unix_command | unix_command\n\tPipes output of first command into input of second command.\n";
    dsh_usage += "\n";
    dsh_usage += server_usage = "unix_command )) <port>\n\tSends the output of the unix_command to the specified port.\n";
    dsh_usage += "\n";
    dsh_usage += client_usage = "unix_command (( <server IP> <port>\n\tConnects a unix_command to a server and takes input from it.\n";
    dsh_usage += "\n";


    


    //Be prepared to catch all signals.
    for (i = 0; i<64; i++)
    {
        //Separate signal handler for signals that try to kill/stop/quit the current process.
        if (i == SIGTERM || i == SIGKILL || i == SIGSTOP || i == SIGINT || i == SIGQUIT)
            (void) signal( i, signalDelMbox);
        else
            (void) signal(i, signalCatch);
    }




    //The mail while loop of the program. Each time you enter a command you will
    //pass through this while loop once.
    while(!should_exit)
    {
        cout << promt;
        cin.getline(cmd, 512);

        vector<string> args;
        args = split(cmd, ' ');

        if (!args.empty())
        {
//+++++++++++++++++CMDNM+++++++++++++++++++++++++++++++++++++++++++++++++++++++
            if (args[0] == "cmdnm")
            {
                //Make sure there is a process Id to look up.
                if (args.size() >= 2)
                {
                    //Convert the process id that the user gave us to an integer.
                    procId = atol(args[1].c_str());
                    print_cmdnm(procId);
                }
                else
                {
                    cout << cmdnm_usage;
                }
            }
//+++++++++++++++++SYSTAT++++++++++++++++++++++++++++++++++++++++++++++++++++++
            else if(args[0] == "systat")
            {
                //Call function that prints system statistics.
                print_systat();
            }
//+++++++++++++++++SIGNAL++++++++++++++++++++++++++++++++++++++++++++++++++++++
            else if(args[0] == "signal")
            {
                //Need a process ID and a signal.
                if (args.size() >= 3)
                {
                    //Convert the process id that the user gave us to an integer.
                    procId = atol(args[1].c_str());
                    sig = atol(args[2].c_str());

                    sendSignal(procId, sig);
                }
                else
                {
                    cout << signal_usage;
                }
            }
//+++++++++++++++++PID+++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            else if(args[0] == "pid") {
                if (args.size() >= 2)
                {  
                    procs = findProcesses(args[1]);
                    printProcesses(procs);
                    //printPid(args[1]);
                }
                else
                {
                    cout << pid_usage;
                }
            }
//+++++++++++++++++CD++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            else if(args[0] == "cd")
            {
                if (args.size() >= 2)
                {  
                    result = chdir(args[1].c_str());
                    if(result != 0)
                    {
                        cout << "error opening " << args[1] << endl;
                    }
                }
                else
                {
                    cout << cd_usage;
                }
            }
//+++++++++++++++++MBOXINIT+++++++++++++++++++++++++++++++++++++++++++++++++++++
            else if(args[0] == "mboxinit")
            {
                if (args.size() > 2)//Need 2 arguments; <num_mailboxes> <size>
                {
                    numBoxes = atol(args[1].c_str());
                    size = atol(args[2].c_str());
                    result = mboxinit(numBoxes, size);
                    if(result == -1)
                    {
                        cout << "Error initializing mailbox" << endl;
                    }
                }
                else
                {
                    cout << mboxinit_usage;
                }
            }
//+++++++++++++++++MBOXDEL++++++++++++++++++++++++++++++++++++++++++++++++++++++
            else if(args[0] == "mboxdel")
            {
                if (args.size() > 1)//Check for forcrefull removal.
                {
                    if (args[1] == "-f")
                        mboxdel(true);
                }
                else
                {
                    //This prevents processes from staying attached.
                    mboxdel(false);
                }
            }
//+++++++++++++++++MBOXWRITE++++++++++++++++++++++++++++++++++++++++++++++++++++
            else if(args[0] == "mboxwrite")
            {
                if (args.size() > 1)
                {
                    boxNum = atol(args[1].c_str());
#if DEBUG
    start_WriteProtection(boxNum);
#endif
                    result = mboxwrite(boxNum);
#if DEBUG
    for(int i=0;i<10;i++){printf("%d\n",i);sleep(1);}
    end_WriteProtection(boxNum);
#endif
                    if (result == -1)
                    {
                        cout << "Error writing to mailbox" << endl;
                    }
                }
                else
                {
                    cout << mboxwrite_usage;
                }
            }
//+++++++++++++++++MBOXREAD+++++++++++++++++++++++++++++++++++++++++++++++++++++
            else if(args[0] == "mboxread")
            {
                if (args.size() > 1)
                {
                    boxNum = atol(args[1].c_str());
#if DEBUG
    start_ReadProtection(boxNum);
#endif
                    result = mboxread(boxNum);
#if DEBUG
    for(int i=0;i<10;i++){printf("%d\n",i);sleep(1);}
    end_ReadProtection(boxNum);
#endif
                    if (result == -1)
                    {
                        cout << "Error reading from mailbox" << endl;
                    }
                }
                else
                {
                    cout << mboxread_usage;
                }
            }
//+++++++++++++++++MBOXCOPY+++++++++++++++++++++++++++++++++++++++++++++++++++++
            else if(args[0] == "mboxcopy")
            {
                if (args.size() > 2)//Need 2 arguments; <boxSource> <boxDest>
                {
                    boxSoNum = atol(args[1].c_str());
                    boxDestNum = atol(args[2].c_str());

#if DEBUG
    start_ReadProtection(boxSoNum);
    start_WriteProtection(boxDestNum);
#endif
                    result = mboxcopy(boxSoNum, boxDestNum);
#if DEBUG
    for(int i=0;i<10;i++){printf("%d\n",i);sleep(1);}
    end_ReadProtection(boxSoNum);
    end_WriteProtection(boxDestNum);
#endif
                    if (result == -1)
                    {
                        cout << "Error copying mailbox" << endl;
                    }
                }
                else
                {
                    cout << mboxcopy_usage;
                }
            }
//+++++++++++++++++++MBOXINFO++++++++++++++++++++++++++++++++++++++++++++++++++
            else if(args[0] == "mboxinfo")
            {
                if (args.size() > 1)
                {
                    boxNum = atol(args[1].c_str());
                    mboxinfo(boxNum);
                }
            }
//+++++++++++++++++++HELP++++++++++++++++++++++++++++++++++++++++++++++++++++++
            else if(args[0] == "--help" || args[0] == "help")
            {
                cout << dsh_usage;
            }
//+++++++++++++++++EXIT++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            else if(args[0] == "exit")
            {
                should_exit = true;
                //Prevents processes from staying attached.
                mboxdel(false);
                cout << "Bye-bye!\n";
            }
//+++++++++++++++++All OTHER COMMANDS++++++++++++++++++++++++++++++++++++++++++
            else
            {
                pipeloc =  contains ("|", args);//Check for pipes.
                greaterThanLoc = contains (">", args);//Check for output redirection.
                lessThanLoc = contains ("<", args);//Check for input redirection.
                dblClsParenLoc = contains ("))", args);//Check for remote pipe serve.
                dblOpnParenLoc = contains ("((", args);//Check for remote pipe client.

                //Pipe.
                if (pipeloc != 0)
                {
                    pipeAndFork(args, pipeloc);
                }
                //Re-direct file output.
                else if (greaterThanLoc != 0)
                {
                    fileReDirectOut(args, greaterThanLoc);
                }
                else if (lessThanLoc != 0)
                {
                    fileReDirectIn(args, lessThanLoc);
                }
                //Remote pipes.
                else if (dblClsParenLoc != 0)
                {
                    remoteServe(args, dblClsParenLoc);
                }
                else if (dblOpnParenLoc != 0)
                {
                    remoteClient(args, dblOpnParenLoc);
                }

                //Anything else execute as a normal command.
                //and print out usage.
                else
                {
                    execCmd(args);
                }
            }

            //Clear the previous arguments so we are ready to take in
            //new arguments.
            args.clear();
        }
    }
    return 0;
}


/**************************************************************************//**
 * @author Andrew Pierson
 *
 * @par Description: This is for cathing all signals. Once a signal is caught
 * it's behavior will be set back to normal. This means that a signal will
 * only be caught once.
 *
 *
 * @param[in] sig - A signal to be caught.
 *
 * @returns none
 *****************************************************************************/
void signalCatch(int sig)
{
    //Signal has been caught, let the user know.
    printf("Received signal %d: %s. From now on signal %d will exhibit default behavior.\n", sig, strsignal(sig), sig);
    //Once the signal has been caught once, set it back to it's normal default behavior.
    (void) signal(sig, SIG_DFL);
}

/**************************************************************************//**
 * @author Andrew Pierson
 *
 * @par Description: This is for cathing signals that try to kill the process.
 * When it catches one of thsese signals it will clean up shared memory.
 *
 *
 * @param[in] sig - A signal to be caught.
 *
 * @returns none
 *****************************************************************************/
void signalDelMbox(int sig)
{
    //Signal has been caught, let the user know.
    printf("Received signal %d: %s. From now on signal %d will exhibit default behavior..\n", sig, strsignal(sig), sig);
    //Once the signal has been caught once, set it back to it's normal default behavior.
    mboxdel(false);
    (void) signal(sig, SIG_DFL);
}







